# [0.3.0](https://gitlab.com/opinionated-digital-center/pyadr/compare/v0.2.0...v0.3.0) (2020-04-03)


### Features

* initialise an adr repository ([565159b](https://gitlab.com/opinionated-digital-center/pyadr/commit/565159b928ba2500241ac4211ecf345f75310842))

# [0.2.0](https://gitlab.com/opinionated-digital-center/pyadr/compare/v0.1.0...v0.2.0) (2020-04-03)


### Features

* generate a table of content ([24674e8](https://gitlab.com/opinionated-digital-center/pyadr/commit/24674e8230efbfba15bf488114120a7a9c9e1c1f))

# [0.1.0](https://gitlab.com/opinionated-digital-center/pyadr/compare/v0.0.0...v0.1.0) (2020-04-01)


### Features

* accept or reject a proposed adr ([f7db80c](https://gitlab.com/opinionated-digital-center/pyadr/commit/f7db80c62a9a3b48062550d34581e223310a9ca6))
